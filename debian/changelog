python-gammu (2.12-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:16:12 -0500

python-gammu (2.12-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Michal Čihař ]
  * Add Python Modules Packaging Team as uploader.
  * Drop Python 2 package, the only reverse dependency was removed
    (Closes: #937776).

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Feb 2020 08:11:25 +0100

python-gammu (2.12-1) unstable; urgency=medium

  [ Michal Čihař ]
  * New upstream release.
  * Bump standards to 4.3.0.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

 -- Michal Čihař <nijel@debian.org>  Sat, 23 Feb 2019 10:15:42 +0100

python-gammu (2.11-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 4.1.3.

 -- Michal Čihař <nijel@debian.org>  Fri, 05 Jan 2018 15:14:35 +0100

python-gammu (2.10-1) unstable; urgency=medium

  * New upstream release.
    - Fixes build with current Gammu (Closes: #880259).
  * Bump standards to 4.1.1.

 -- Michal Čihař <nijel@debian.org>  Tue, 07 Nov 2017 09:10:12 +0100

python-gammu (2.9-1) unstable; urgency=medium

  * New upstream release.
  * Switch Vcs-Git URL to https.
  * Bump standards to 4.0.0.

 -- Michal Čihař <nijel@debian.org>  Mon, 10 Jul 2017 20:17:45 +0200

python-gammu (2.7-1) unstable; urgency=medium

  * New upstream release.
  * Depends on Gammu 1.37.90 or newer.

 -- Michal Čihař <nijel@debian.org>  Sun, 23 Oct 2016 15:13:02 +0200

python-gammu (2.6-1) unstable; urgency=medium

  * New upstream release.
  * Add dh-python to Build-Depends.
  * Bump standards to 3.9.8.
  * Build depend on Gammu 1.37.3 to avoid possible crashes in testsuite.

 -- Michal Čihař <nijel@debian.org>  Thu, 26 May 2016 09:10:22 +0200

python-gammu (2.5-1) unstable; urgency=medium

  * New upstream release.
    - Compatible with current Gammu (Closes: #811492).
  * Add forgotten Vcs-* headers to debian/control.

 -- Michal Čihař <nijel@debian.org>  Tue, 19 Jan 2016 15:37:46 +0100

python-gammu (2.4-1) unstable; urgency=medium

  * New upstream release.
    - Fixes FTBFS on s390x and ppc64.
    - Patch merged upstream.

 -- Michal Čihař <nijel@debian.org>  Wed, 02 Sep 2015 10:24:23 +0200

python-gammu (2.3-2) unstable; urgency=medium

  * Upload to unstable.

 -- Michal Čihař <nijel@debian.org>  Tue, 01 Sep 2015 18:30:45 +0200

python-gammu (2.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/copyright to match upstream licensing.

 -- Michal Čihař <nijel@debian.org>  Wed, 03 Jun 2015 15:37:07 +0200

python-gammu (2.1-1) experimental; urgency=medium

  * New upstream release.
  * Enable build time testsuite.
  * Install upstream changelog.

 -- Michal Čihař <nijel@debian.org>  Wed, 15 Apr 2015 10:12:26 +0200

python-gammu (2.0-1) experimental; urgency=low

  * New source package split from Gammu by upstream.
  * Supports python 3 now.

 -- Michal Čihař <nijel@debian.org>  Tue, 14 Apr 2015 10:36:55 +0200
